FactoryGirl.define do
  factory :record do
    title "MyString"
    date "2016-02-29"
    amount 1.5
  end
end
