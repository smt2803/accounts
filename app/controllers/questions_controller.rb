class QuestionsController < ApplicationController
  before_action :load_guestion, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:idex, :show]

  def index
    @questions = Question.all
  end

  def show
  end

  def new
    @question = Question.new
  end
  
  def edit
  end

  def create
    @questions = Question.new(questions_params)
    if @question.save
      redirect_to @question
    else
      render :new
    end
  end

  def update
    if @question.update(questions_params)
      redirect_to @question
    else
      render :edit
    end
  end

  def destroy
    @question.destroy
    redirect_to question_path
  end

  private

  def load_guestion
    @question = Question.find(params[:id])
  end

  def questions_params
    params.require(:question).permit(:title, :body)
  end

end
